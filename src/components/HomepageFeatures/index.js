import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'The GeoMapFish frontend',
    image: 'img/geomapfish.png',
    description: (
      <>
        GeoGirafe was designed to replace the current frontend of GeoMapFish.
        It is based on modern technologies, and is 100% compatible with the <a href="https://github.com/camptocamp/c2cgeoportal/">GeoMapFish backend</a>.
      </>
    ),
  },
  {
    title: 'Enter the community',
    image: 'img/community.png',
    description: (
      <>
        GeoGirafe is managed by an <a href="https://geomapfish.org">active and independent community</a> of 
        public, private and academic entities, who meet weekly to ensure the product's ongoing, sustainable development.
      </>
    ),
  },
  {
    title: 'Powered by VanillaJS',
    image: 'img/vanillajs.png',
    description: (
      <>
        GeoGirafe is simple, reactive, accessible, secure and easy to learn. 
        All based on pure standard Javascript.
        Check our <a href="">GitLab Repository</a> for more information.
      </>
    ),
  }
];

function Feature({image, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <img className={styles.featureImg} src={image} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
