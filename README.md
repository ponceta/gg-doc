# GeoGirafe Help Website

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

  `npm install`  
    Install dependencies.

  `npm start`  
    Starts the development server.
    (Can be extremely long on Windows)

  `npm run build`  
    Bundles your website into static files for production.

  `npm run serve`  
    Serves the built website locally.

  `npm deploy`  
    Publishes the website to GitHub pages.
